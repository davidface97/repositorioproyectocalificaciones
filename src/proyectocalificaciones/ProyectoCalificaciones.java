package proyectocalificaciones;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
public class ProyectoCalificaciones {

   

Scanner teclado= new Scanner(System.in);
        
    Universidad universidad= new Universidad();
    
    public void index() throws FileNotFoundException, DocumentException{
        int tester=0;
        while(tester!=2){
    System.out.println("Elija una opcion: "
            + "\n" +"1. Ingresar como Admin"
            +"\n"  +"2. Crear nuevo profesor"
            + "\n" +"3. Ingresar como un profesor"
            + "\n" +"4. Salir"
            + "\n" +"Opcion -> ");
    int primermenu=teclado.nextInt();
    
    switch(primermenu){
        case 1:
            this.ConfigAdmin();
            break;
        case 2:
            this.CreateTeacher();
            break;
        case 3:
            this.LoginTeacher();
            break;
        case 4:
            tester=2;
            break;
    }
    }
    } 

//    for (int i = 0; i < puntosC; i++) {
//        System.out.println("Cuantos subpuntos tiene el punto "+(i+1));
//        int respuestasubpuntos=teclado.nextInt();
//        for (int j = 0; j < respuestasubpuntos; j++) {
//            System.out.println("Que porcentaje tiene el subpunto "+ (j+1)+" para el punto "+(i+1));
//            int porcentajesub=teclado.nextInt();
//
//        }
//        System.out.println("Que porcentaje tiene el punto "+(i+1));
//        int porcentajepun=teclado.nextInt();
//    }
    

        
public void ConfigAdmin () throws InputMismatchException, IndexOutOfBoundsException{
        int tester=0;
        while(tester!=2){
        System.out.println("Ingrese la clave numerica de Configuracion"
                + "\n" +"-> ");
        int test=teclado.nextInt();
        
        if(test==universidad.clave){
            System.out.println("_______Clave Correcta| Bienvenido Administrador_______");
            System.out.println("1. Agregar Materia");
            System.out.println("2. Agregar Estudiante");
            System.out.println("3. Salir");
            System.out.println("-> ");
            int respuestaadmin=0;
            try{
            respuestaadmin=teclado.nextInt();
            }catch(InputMismatchException e){
                System.out.println("Ingrese un dato valido...");
            }
            switch(respuestaadmin){
                case 1:
                    
                    System.out.println("Nombre Materia: ");
                    String nombremateria=teclado.next();
                    System.out.println("ID Materia");
                    int IDmateria= teclado.nextInt();
                    
                    Materia materia= new Materia(nombremateria, IDmateria);
                    universidad.addMateriaUniversidad(materia);
                    break;
                case 2:
                    System.out.println("Nombre Estudiante");
                    String nombreestudiante=teclado.next();
                    System.out.println("ID Estudiante");
                    int IDestudiante=teclado.nextInt();
                    Estudiante estudainte=new Estudiante(nombreestudiante, IDestudiante);
                    universidad.addEstudianteUniversidad(estudainte);
                    break;
                case 3:
                    tester=2;
                    break;
            }
} else{
            System.out.println("Clave Incorrecta, vuelva a intentarlo...");
        }
}
    }
    
    public void LoginTeacher() throws FileNotFoundException, DocumentException{
     boolean comprobadorIDcorrecto=false;
            
            while(comprobadorIDcorrecto==false){
//            System.out.println("Ingrese su Nombre");
//            int Idlogin=teclado.nextInt();
            
            for(int i=0; i<universidad.getProfesores().size();i++){  
                System.out.println(universidad.getProfesores().get(i).getNombre());
            }//for
            System.out.println("Seleccione un Profesor");
                String eleccionProfesor=teclado.next();
            for(int i=0; i<universidad.getProfesores().size();i++){
                
                
            if(universidad.profesores.get(i).getNombre().equals(eleccionProfesor)){
                System.out.println("Ingrese su clave "+ eleccionProfesor
                        + "\n"+"-> ");
                String clavelogin=teclado.next();
                if(universidad.profesores.get(i).getClave().equals(clavelogin)){
                    System.out.println("Hola "+eleccionProfesor);
                    System.out.println("");
                    comprobadorIDcorrecto=true;
                }//if
            }//if
            
            }//for 
//            }//while
            int menuprofesor=0;
            while(menuprofesor!=2){
            System.out.println("Que desea hacer? "
                    + "\n"+"1. Vincular Materia"//Vincula las materias de Universidad con el profesor
                    + "\n"+"2. Vincular Estudiantes a Materia"//Vincula los estudiantes de la universidad con la materia del profesor 
                    + "\n"+"3. Crear trabajo"//Debe preguntar a que Materia, Distribucion de puntos y porcentajes
                    + "\n"+"4. Calificar Trabajo"// Entra a Materia, y a estudiante
                    + "\n"+"5. Revisar Notas estudiante"//Entra a estudiante especifico, en una materia especifica en un trabajo especifico, puede calcular nota final de la materia, o promedio en el semestre
                    + "\n"+"6. Salir");//Pregunta materia, y calcula notas, puede calcular promedio
//                    + "\n"+"7. Salir");
            int menuProfesor= teclado.nextInt();
//                System.out.println("Ingrese su nombre");
//                String usuario=teclado.next();
            switch(menuProfesor){
                case 1:
                    this.relacionMaterias(eleccionProfesor);
                    break;
                case 2:
                    this.relacionEstudiantes(eleccionProfesor);
                    break;
                case 3:
                    this.crearTrabajo(eleccionProfesor);
                    break;
                case 4:
                    this.calificarTrabajo(eleccionProfesor);
                    break;
                case 5:
                    this.revisionNotasEstudiante(eleccionProfesor);
                    break;
                case 6:
                    menuprofesor=2;
//                    this.notasCurso();
                    break;
                case 7:
                    break;
            }
    }  
    }//tester
    }
    
    public void relacionMaterias(String usuario){
        boolean a=true;
        int salir=0;
        do{
        System.out.println("Que materia desea vincular a su actividad laboral dentro de la universidad? ");
        for (int i = 0; i < universidad.materias.size(); i++) {
            System.out.println(universidad.materias.get(i).getNombre());
        }
        String vinculomateria= teclado.next();
        for (int i = 0; i < universidad.materias.size(); i++) {
        if(universidad.materias.get(i).getNombre().equals(vinculomateria)){
            for(int x=0; x<universidad.getProfesores().size();x++){
                a=false;
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    Materia materiaprofesor=universidad.getMaterias().get(i);
                    universidad.profesores.get(x).addMateriaProfesor(materiaprofesor);
                    System.out.println("***********Materia Vinculada***********");
                    a=true;
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getNombre());
                    } //temp----------------------------------------------------------------------------------------------------------------------
                    System.out.println("");
                    System.out.println("********************************************");
                }
            }
            
        }else{
            if(a==false){
            System.out.println("No tiene registrada la materia "+vinculomateria+" iniciando otro registro");
            }
        }
//        a++;
        }
            System.out.println("Desea agregar otra materia? 1. Si 2. No");
            salir=teclado.nextInt();
        }while(salir!=2);
        
        
    }
    
    public void relacionEstudiantes(String usuario){
        boolean a=true;
        int b=0;
        int salir=0;
        do{
            System.out.println("A cual de sus materias desea inscribir estudiantes? ");
           
            for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getNombre());
                    }
                }
            }
            System.out.println("Seleccione Una: ");
            String materiadictada=teclado.next();//despues de mostrar
            for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        if(universidad.profesores.get(x).getMaterias().get(j).getNombre().equals(materiadictada)){
                            System.out.println("Que estudiante desea inscribir? ");
                            for (int i = 0; i < universidad.estudiantes.size(); i++) {
                                System.out.println(universidad.estudiantes.get(i).getNombre());
                            }
                            String estudiante=teclado.next();
                            for (int i = 0; i < universidad.estudiantes.size(); i++) {
                                if(universidad.estudiantes.get(i).getNombre().equals(estudiante)){
                                    a=true;
                                    Estudiante estudianteagregar=universidad.estudiantes.get(i);
                                    universidad.getProfesores().get(x).getMaterias().get(j).addEstudiante(estudianteagregar);
                                    System.out.println("***********Estudiante Agregado***********");
                                    for (int k = 0; k < universidad.getProfesores().get(x).getMaterias().get(j).getEstudiantes().size() ; k++) {
                                        System.out.println(universidad.getProfesores().get(x).getMaterias().get(j).getEstudiantes().get(k).getNombre());
                                    }
                                    System.out.println("********************************************");
                                }else{
                                    if(a!=true){
                                    System.out.println("Estudiante no encontrado, iniciando nuevo registro...");
                                    }
                                }
                            } 
                        }else{
                            if(b>universidad.profesores.get(x).getMaterias().size()){
                            System.out.println("No tiene la materia "+materiadictada+"registrada a su Usuario, iniciando nuevo registro...");
                            }
                        }
                    }
                }
            }
            System.out.println("Desea agregar mas estudiantes? 1. Si 2. No");
            salir=teclado.nextInt();
        }while(salir!=2);
        
    }
    
    public void crearTrabajo(String usuario){
        int salir=0;
       do{
           
           System.out.println("A cual de sus materias desea agregar el trabajo? ");
           for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getNombre());
                    }
                }
            }
            String materiadictada=teclado.next();
            
            for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        if(universidad.profesores.get(x).getMaterias().get(j).getNombre().equals(materiadictada)){
                            Materia materiaa=universidad.profesores.get(x).getMaterias().get(j);
                            System.out.println("Cual es el nombre para el trabajo?");
                            String nombre=teclado.next();
                            System.out.println("Cuantos Puntos Tiene el trabajo?");
                            int puntos=teclado.nextInt();
                            System.out.println("Cual es el porcentaje en el corte de este trabajo /100?");///////CALCULAR EL PORCENTAJE DEL CORTE
                            double porcentaje=teclado.nextDouble();
                            Trabajo trabajo=new Trabajo(materiaa,puntos,porcentaje,nombre);//*****************************************************
                            universidad.getProfesores().get(x).getMaterias().get(j).addTrabajo(trabajo);
                            System.out.println("Trabajo Agregado");
                            System.out.println("Distribuya los porcentajes dentro del trabajo");
//                            Puntos[] punto= new Puntos[puntos];
                            for (int puntoespecifico = 0; puntoespecifico < puntos; puntoespecifico++) {
                                System.out.println("El punto "+(puntoespecifico+1)+" tiene Subpuntos? 1.Si 2.No");
                                int desicion=teclado.nextInt();
                                if(desicion==1){
                                    System.out.println("Cuantos Subpuntos Tiene");
                                    int cantidadsubpuntos=teclado.nextInt();
                                    int sumador=100;
                                    double porcentajepunto=0;
                                    double resta=(100-porcentajepunto);
                                    System.out.println("Que porcentaje tiene para el trabajo el punto "+(puntoespecifico+1)+" /"+sumador+" no puede ser mayor a "+resta);
                                    System.out.println("--------------->>>> ");
                                    porcentajepunto=teclado.nextDouble();
                                    Puntos puntoAagregar= new Puntos(cantidadsubpuntos, porcentajepunto);
                                    trabajo.punto.add(puntoAagregar);//***************************************************************************

                                     
                                        double porcentajesubpunto=0;//Cuidado
                                        double tester=0;
                                    for (int i = 0; i < cantidadsubpuntos; i++) {
                                        

                                               tester=tester+porcentajesubpunto;
                                        System.out.println("Que porcentaje tiene el subpunto "+(i+1)+" para el punto "+(puntoespecifico+1)+" (No puede ser mayor a  "+(porcentajepunto-tester)+" )");
                                        porcentajesubpunto=teclado.nextDouble();//Cuidado
//                                      double porcentajesubpunto=teclado.nextDouble();//Original
                                        if(porcentajesubpunto<porcentajepunto){
                                        SubPuntos subpunto= new SubPuntos(porcentajesubpunto);
                                        double revisorsubpuntos=0;
                                        double acumuladorsubpuntos=0;
                                        revisorsubpuntos=porcentajesubpunto;
                                        acumuladorsubpuntos=acumuladorsubpuntos+revisorsubpuntos;
                                        if(acumuladorsubpuntos<=porcentajepunto){
                                        trabajo.punto.get(puntoespecifico).addSubPunto(subpunto);

                                        }else{
                                            System.out.println("En el subpunto"+(i+1)+"Sobrepaso el porcentaje de "+porcentajepunto+" del punto..."+(puntoespecifico+1));
                                            tester=0;
                                            porcentajesubpunto=0;
                                            i=0;
                                        }
                                        }else{
                                            System.out.println("Sobrepaso el porcentaje de "+porcentajepunto+" del punto"+(puntoespecifico+1));
                                            tester=0;
                                            porcentajesubpunto=0;
                                            i=0;
                                        } 
                                    }
                                    
                                }else{
                                    System.out.println("Que porcentaje tiene para el trabajo el punto "+(puntoespecifico+1));
                                    double porcentajepunto=teclado.nextDouble();
                                    Puntos PuntoAgregar= new Puntos(0, porcentajepunto);//El numero de SubPuntos es 0, tener cuidado a la hora del calculo de la nota
                                    trabajo.punto.add(PuntoAgregar);
                                }
                            }
                            for (int i = 0; i < universidad.getProfesores().get(x).getMaterias().get(j).getEstudiantes().size(); i++) {
                                universidad.getProfesores().get(x).getMaterias().get(j).getEstudiantes().get(i).addTrabajo(trabajo);
                                System.out.println("Trabajo Agregado a Estudiante "+ universidad.getProfesores().get(x).getMaterias().get(j).getEstudiantes().get(i).getNombre());
                            }
                        }
                    }
                }
            }      
           
           System.out.println("Desea agregar otro trabajo? 1. Si 2. No");
           salir=teclado.nextInt();
        }while(salir!=2);
    }

    public void calificarTrabajo(String usuario){
        int salir=0;
        do {            
        System.out.println("A cual de sus materias desea calificar un trabajo? ");
           for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getNombre());
                    }//texto
                }
            }
            String materiadictada=teclado.next();
            
             for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        if(universidad.profesores.get(x).getMaterias().get(j).getNombre().equals(materiadictada)){
                            System.out.println("Que trabajo desea calificar?");
                            for (int p = 0; p < universidad.profesores.get(x).getMaterias().get(j).getTrabajos().size(); p++) {
                                System.out.println(universidad.profesores.get(x).getMaterias().get(j).getTrabajos().get(p).getNombreTrabajo());
                            }//texto
                            String TrabajoDeseado=teclado.next();
                            for (int i = 0; i < universidad.profesores.get(x).getMaterias().get(j).getTrabajos().size(); i++) {
                                if(universidad.profesores.get(x).getMaterias().get(j).getTrabajos().get(i).getNombreTrabajo().equals(TrabajoDeseado)){
                                    System.out.println("A que estudiante desea calificar el trabajo ' "+TrabajoDeseado+" '");
                                    for (int k = 0; k < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().size(); k++) {
                                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getNombre());
                                    }
                                    String EstudianteDeseado=teclado.next();
                                    for (int k = 0; k < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().size(); k++) {
                                    if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getNombre().equals(EstudianteDeseado)){
                                        System.out.println("El estudiante "+ EstudianteDeseado+" tiene los siguientes trabajos: ");
                                        for (int l = 0; l < universidad.getEstudiantes().get(k).getTrabajos().size(); l++) {
                                            System.out.println(universidad.getEstudiantes().get(k).getTrabajos().get(l).getNombreTrabajo());
                                        }
                                        System.out.println("Rectifique que trabajo desea calificar: ");
                                        String trabajodeestudiantequedeseacalificar=teclado.next();
                                        
                                        for (int q = 0; q < universidad.getEstudiantes().get(k).getTrabajos().size(); q++) {
                                            if(universidad.getEstudiantes().get(k).getTrabajos().get(q).getNombreTrabajo().equals(trabajodeestudiantequedeseacalificar)){
                                                
                                            for (int n = 0; n < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().size(); n++) {
                                            if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().get(n).getSubpunto().size()>0){
                                                for (int m = 0; m < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().get(n).getSubpunto().size(); m++) {
                                                    System.out.println("Que calificacion tiene el subpunto "+(m+1)+" en el punto "+(n+1)+" para el estudiante "+EstudianteDeseado);
                                                    double calificacionSubPunto=teclado.nextDouble();
                                                    universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().get(n).getSubpunto().get(m).setCalificacion(calificacionSubPunto);
                                                }
                                            }else{
                                                //Calculo nota punto sin subpuntos
                                            System.out.println("Cual es la calificacion para "+EstudianteDeseado+" en el punto "+(n+1)+" del trabajo "+TrabajoDeseado);
                                            double calificacionpuntoset=teclado.nextDouble();
                                            double calculopuntosolo=((universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().get(n).getPorcentajePunto())/100)*(calificacionpuntoset);
                                            universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(q).getPunto().get(n).setCalificacion(calculopuntosolo);
                                            }
                                                
                                            }
                                            ////////////////////////////////////////////////////////////////////////////////////////
                                            
                                            double notapunto2=0;
                                        for (int l = 0; l < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().size(); l++) {
                                            if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size()>0){
                                                double nota =0;
                                                double nota2=0;
                                                for (int m = 0; m < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size(); m++) {
                                                nota=(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().get(m).getPorcentajeSubPunto()/100)*(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().get(m).getCalificacion());
                                                    System.out.println("Nota subpunto "+nota );
                                                nota2=nota2+nota;
                                                    System.out.println("Nota subpunto Nota2"+nota2+ (m+1));
                                                }
                                                universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).setCalificacion(nota2);
                                                System.out.println("NOTA CALCULADA SUMATORIA SUBPUNTOS: "+nota2);
                                                                    //nota2 es la nota ya calculada del punto
                                            }else{
                                                double notapunto=((universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getPorcentajePunto()/100)*(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getCalificacion()));
                                                double a=universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getPorcentajePunto()/100;
                                                double b=universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getCalificacion();
                                                double calculator= a*b;
                                                System.out.println("Calculator"+calculator);
                                                System.out.println("Dividido 100 "+universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getPorcentajePunto()/100);
                                                System.out.println("GetCalificacion "+universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getCalificacion());
                                                System.out.println("");
                                                System.out.println("Nota punto "+notapunto);
                                                notapunto2=notapunto2+notapunto;
                                                System.out.println("Nota punto 2 "+notapunto2);
                                                universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).setCalificacion(notapunto2);
                                            }
                                         }
                                         double notaa =0;
                                         double notaa2=0;
                                         for (int l = 0; l < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().size(); l++) {
                                            notaa=universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getCalificacion();
                                             System.out.println("Nota GetCalificacion "+notaa);
                                            notaa2=notaa2+notaa;///////////////////////////////////////
                                             
                                        }
                                            System.out.println("La nota seria: "+notaa2);//Nota Trabajo X
                                            universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).setCalificacion(notaa2);//Asignar calificacion a trabajo
////////////////////////////////////////////enviarcorreo(correo);
                                                System.out.println("Para enviar un correo al estudiante ingrese el correo del estudiante: ");
                                                String correoestudiante=teclado.next();
                                                System.out.println("Agrege el Asunto del Correo: ");
                                                String asuntocorreo=teclado.next();
                                                System.out.println("Ingrese el texto del mensaje");
                                                String textomensaje=teclado.next();
//////                                                this.enviarcorreo(correoestudiante, asuntocorreo, textomensaje, notaa2);

                                            //////////////////////////////////////////////////////////////////////////
                                        }
                                    }
                                    }
                                    
//                                    
//                                    for (int k = 0; k < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().size(); k++) {
//                                    if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getNombre().equals(EstudianteDeseado)){
//                                        for (int l = 0; l < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().size(); l++) {
//                                            if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size()>0){
//                                                for (int m = 0; m < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size(); m++) {
//                                                    System.out.println("Que calificacion tiene el subpunto "+(m+1)+" en el punto "+(l+1)+" para el estudiante "+EstudianteDeseado);
//                                                    double calificacionSubPunto=teclado.nextDouble();
//                                                    universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().get(m).setCalificacion(calificacionSubPunto);
//                                                }
//                                            }else{
//                                                //Calculo nota punto sin subpuntos
//                                            System.out.println("Cual es la calificacion para "+EstudianteDeseado+" en el punto "+(l+1)+" en el trabajo "+TrabajoDeseado);
//                                            double calificacionpuntoset=teclado.nextDouble();
//                                            double calculopuntosolo=((universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getPorcentajePunto())/100)*(calificacionpuntoset);
//                                            universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).setCalificacion(calculopuntosolo);
//                                            }
//                                        }  
//                                        //Calculo Calificacion Trabajo Subpuntos
//                                         for (int l = 0; l < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().size(); l++) {
//                                            if(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size()>0){
//                                                double nota =0;
//                                                double nota2=0;
//                                                for (int m = 0; m < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().size(); m++) {
//                                                nota=((universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().get(m).getPorcentajeSubPunto())/100)*(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getSubpunto().get(m).getCalificacion());
//                                                nota2=nota2+nota;
//                                                }
//                                                universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).setCalificacion(nota2);
//                                                //nota2 es la nota ya calculada del punto
//                                            }
//                                         }
//                                         double notaa =0;
//                                         double notaa2=0;
//                                         for (int l = 0; l < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().size(); l++) {
//                                            notaa=universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).getPunto().get(l).getCalificacion();
//                                            notaa2=notaa2+notaa;///////////////////////////////////////
//                                             
//                                        }
//                                            System.out.println("La nota seria: "+notaa2);//Nota Trabajo X
//                                         universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(k).getTrabajos().get(i).setCalificacion(notaa2);//Asignar calificacion a trabajo
//                                    }
//                                    }
                                } 
                           }
                            
                        }
                    }else{
                            System.out.println("No tiene la materia vinculada...");
                        }
                }
             } 
        } 
           System.out.println("Desea calificar otro trabajo? 1. Si 2. No");
           salir=teclado.nextInt();
        }while(salir!=2);
        }
    public void revisionNotasEstudiante(String usuario) throws FileNotFoundException, DocumentException{
        int salir=0;
        do {            
            System.out.println("En que materia desea consultar notas?");
            for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        System.out.println(universidad.profesores.get(x).getMaterias().get(j).getNombre());
                        }
                    }
                }//Mostrar
            String materiadictada=teclado.next();
            for(int x=0; x<universidad.getProfesores().size(); x++){  
                if(universidad.getProfesores().get(x).getNombre().equals(usuario)){
                    for (int j = 0; j < universidad.profesores.get(x).getMaterias().size(); j++) {
                        if(universidad.profesores.get(x).getMaterias().get(j).getNombre().equals(materiadictada)){
                            System.out.println("Desea calcular la nota por"
                                    + "\n"+"1. Estudiante"//NOTAS POR ESTUDIANTE EN LA MATERIA
                                    + "\n"+"2. Materia");//PROMEDIOS SUMADOS DE LOS PROMEDIOS DE LOS ESTUDIANTES
                            int eleccion=teclado.nextInt();
                            switch(eleccion){
                                case 1:
                                System.out.println("Que estudiante desea concultar? ");
                                for (int i = 0; i < universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().size(); i++) {
                                    System.out.println(universidad.profesores.get(x).getMaterias().get(j).getEstudiantes().get(i).getNombre());
                                }
                                String estudiantedeseado=teclado.next();
                                for (int p = 0; p < universidad.profesores.get(x).getMaterias().size(); p++) {
                                    for (int jj = 0; jj < universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().size(); jj++){
                                    if(universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(jj).equals(estudiantedeseado)){
                                        for (int i = 0; i <universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(jj).getTrabajos().size() ; i++) {//Entrar en trabajos
                                            System.out.println("Nota "+(i+1)+": "+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(jj).getTrabajos().get(i).getCalificacion()+" en el trabajo '"+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(jj).getTrabajos().get(i).getNombreTrabajo()+" '");
                                        } //Se muestra calificacion por trabajo
                                    }
                                }
                                    }
                                    break;
                                    
                                case 2:
                                double nota1=0;
                                double nota2=0;
                                double promedioestudiante=0;
                                double promediomateria=0;
                                double calculopromedio=0;
                                int contadormateria=0;
                                
                                            System.out.println("En que materia desea consultar notas?");
            for( int xx=0; xx<universidad.getProfesores().size(); xx++){  
                if(universidad.getProfesores().get(xx).getNombre().equals(usuario)){
                    for ( int jjj = 0; jjj < universidad.profesores.get(xx).getMaterias().size(); jjj++) {
                        System.out.println(universidad.profesores.get(xx).getMaterias().get(jjj).getNombre());
                        }
                    }
                }//Mostrar
            materiadictada=teclado.next();
                                for (int p = 0; p < universidad.profesores.get(x).getMaterias().size(); p++) {
                                    if(universidad.profesores.get(x).getMaterias().get(p).equals(materiadictada)){
                                        for (int i = 0; i <universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().size() ; i++) {
                                            for (int k = 0; k <universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().size() ; k++) {
                                                 nota1=(universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().get(k).getCalificacion());
                                                 double nn=(universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().get(k).getPorcentajeenelcorte());
                                                 double calculator=((nota1)*(nn/100));
                                                 nota2=nota2+calculator;
                                            }
                                            promedioestudiante=nota2;
                                            System.out.println("el promedio de "+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getNombre()+" en la materia"+ materiadictada+" es:"+promedioestudiante);
                                            promediomateria=promediomateria+promedioestudiante;
                                            contadormateria++;
                                        }
                                        calculopromedio=promediomateria/contadormateria;
                                        System.out.println("El promedio de la materia es: "+ calculopromedio);  
///////////////////////////////////////////Enviar PDF

        FileOutputStream archivo = new FileOutputStream("Calificaciones.pdf");
		Document documento = new Document();
		PdfWriter.getInstance(documento, archivo);
		documento.open();
		                documento.add(new Paragraph("Calificaciones Curso",
				FontFactory.getFont("arial",   // fuente
				50,                            // tamaño
				Font.BOLDITALIC,               // estilo
				BaseColor.ORANGE)));           // color
		documento.add(new Paragraph(materiadictada, FontFactory.getFont("arial",40,Font.UNDERLINE,BaseColor.DARK_GRAY)));
                documento.add(new Paragraph("Profesor: "+usuario,
				FontFactory.getFont("arial",   // fuente
				22,                            // tamaño
				Font.ITALIC,                   // estilo
				BaseColor.CYAN)));             // color
                
                PdfPTable tabla = new PdfPTable(1);
                for (int i = 0; i < universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().size(); i++){
                    for (int k = 0; k < universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().size(); k++) {
                            tabla.addCell("Estudiante: "+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getNombre()+ " | Trabajo: "+ universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().get(k).getNombreTrabajo()+ " | Calificacion: "+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getTrabajos().get(k).getCalificacion());
                    }   
                }
                documento.add(tabla);
                documento.add(new Paragraph("El promedio de la materia es: "+ (calculopromedio), FontFactory.getFont("arial",50,Font.UNDERLINE,BaseColor.DARK_GRAY)));
                                        for (int i = 0; i < 10; i++) {
                                            documento.add(new Paragraph("El promedio de "+universidad.profesores.get(x).getMaterias().get(p).getEstudiantes().get(i).getNombre()+" en la materia"+ materiadictada+" es:"+promedioestudiante, FontFactory.getFont("arial",50,Font.UNDERLINE,BaseColor.DARK_GRAY)));
                                        }
                documento.close();


                                    }else{
                                        System.out.println("Error .. ..");
                                    }
                                }
                                    
                                    break;
                            }

                            if(eleccion!=1 && eleccion!=2){
                                System.out.println("Ingrese una opcion correcta 1 o 2, reiniciando...");
                            } 
                        }else{
                            System.out.println("Error...");
                        }
                    }
                }
            }

           System.out.println("Desea volver a revisar? 1. Si 2. No");
           salir=teclado.nextInt();
        } while (salir!=2);
        
        
    }
    
//    public void notasCurso(){
//        
//    } 
    public void CreateTeacher(){
        
         System.out.println("Ingrese el nombre");
            String nombre=teclado.next();
            System.out.println("Ingrese Identificacion");
            int IDprofesor=teclado.nextInt();
            System.out.println("Ingrese Clave");
            String claveprofesor=teclado.next();
            
            Profesor profesor= new Profesor(nombre,IDprofesor,claveprofesor);          
            universidad.addProfesorUniversidad(profesor);
            
    }
    public void enviarcorreo(String correoestudiante, String asuntocorreo, String textomensaje, double notaa2){
        try
        {
            // Propiedades de la conexión, configuracion del servicio smtp.
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.user", "davidfelipe.acevedotorres@gmail.com");
            props.setProperty("mail.smtp.auth", "true");

            // Preparamos la sesion
            Session session = Session.getDefaultInstance(props);

            // Construimos el mensaje
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("davidfelipe.acevedotorres@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoestudiante));
            message.setSubject(asuntocorreo);
            message.setText(textomensaje+" Su calificacion es: "+notaa2);

            // Lo enviamos.
            Transport t = session.getTransport("smtp");
            t.connect("davidfelipe.acevedotorres@gmail.com", "agosto12");
            t.sendMessage(message, message.getAllRecipients());

            // Cierre.
            t.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void generarPDF(String materia, String usuario)throws FileNotFoundException, DocumentException{
        FileOutputStream archivo = new FileOutputStream("Calificaciones.pdf");
		Document documento = new Document();
		PdfWriter.getInstance(documento, archivo);
		documento.open();
		                documento.add(new Paragraph("Calificaciones Curso",
				FontFactory.getFont("arial",   // fuente
				50,                            // tamaño
				Font.BOLDITALIC,               // estilo
				BaseColor.ORANGE)));           // color
		documento.add(new Paragraph(materia, FontFactory.getFont("arial",50,Font.UNDERLINE,BaseColor.DARK_GRAY)));
                documento.add(new Paragraph("Profesor: "+usuario,
				FontFactory.getFont("arial",   // fuente
				22,                            // tamaño
				Font.ITALIC,                   // estilo
				BaseColor.CYAN)));             // color
                
                PdfPTable tabla = new PdfPTable(1);
                for (int i = 0; i < 15; i++){
                        tabla.addCell("celda " + i);
                }
                documento.add(tabla);
        	documento.close();
    }
    
    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Universidad maiin= new Universidad();
        ProyectoCalificaciones run= new ProyectoCalificaciones();
        
        run.index();
    }

}
